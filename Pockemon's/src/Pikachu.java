
public class Pikachu implements Fighter {

    private static double pickachuHp = 100;
    private static double pickachuDamage = 10;

    //This ability taking damage 1-4 times(can used 1 times for all game);
    public void ElectricСharge(Fighter defending, Fighter attacking) {
        int shotCount = (int) (Math.random() * 4);
        for(int i = 0; i < shotCount; i++) {
            attacking.attack(defending, attacking);
            System.out.println("Powerful!!!! SlowPoke HP: " + defending.getHp());
        }
    }

    @Override
    public double attack(Fighter defending, Fighter attacking) {
        return defending.takingDamage(attacking);
    }

    @Override
    public double takingDamage(Fighter fighter) {
        return pickachuHp -= fighter.getDamage();
    }

    @Override
    public double getHp() {
        return pickachuHp;
    }

    @Override
    public double getDamage() {
        return pickachuDamage;
    }

}


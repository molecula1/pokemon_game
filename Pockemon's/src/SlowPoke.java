public class SlowPoke implements Fighter {

    private static double slowPokeHp = 110;
    private static double slowPokeDamage = 20;
    private Pikachu pikachu;

    //This ability
    public void psychoSlam(Fighter defending) {
        int randomlyAction = (int) Math.random() + 1;
        if (randomlyAction == 1) {
            System.out.println("Pikachu used a normal attack against himself");
            defending.attack(defending, defending);
            System.out.println("Pikachu HP: " + defending.getHp());
        } else {
            System.out.println("Pikachu used ELECTRIC CHARGE against himself!!!! OMG!");
                pikachu.ElectricСharge(pikachu, pikachu);
                System.out.println("Pikachu HP: " + defending.getHp());
        }
    }

    @Override
    public double attack(Fighter defending, Fighter attacking) {
        return defending.takingDamage(attacking);
    }

    @Override
    public double takingDamage(Fighter fighter) {
        return slowPokeHp -= fighter.getDamage();
    }

    @Override
    public double getDamage() {
        return slowPokeDamage;
    }

    @Override
    public double getHp() {
        return slowPokeHp;
    }
}

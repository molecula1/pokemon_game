public interface Fighter {


    double attack(Fighter defending, Fighter attacking);

    double takingDamage(Fighter fighter);

    double getDamage();

    double getHp();

}

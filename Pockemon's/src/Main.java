import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Pikachu pikachu = new Pikachu();
        SlowPoke slowPoke = new SlowPoke();
        ASCIIpictures pic = new ASCIIpictures();
        Scanner in = new Scanner(System.in);
        int commandForChoosePokemon;
        int commandForAbility;

        System.out.println("Welcome to the game! Choose your Pokemon: \n1-Pikachu\n2-SlowPoke");
        commandForChoosePokemon = in.nextInt();
        while(commandForChoosePokemon <= 0 || commandForChoosePokemon > 2) {
            System.out.println("Unknown pokemon! Choose Pikachu or SlowPoke");
            commandForChoosePokemon = in.nextInt();
        }
        switch (commandForChoosePokemon) {
            case 1:
                System.out.println("Great Choice! Pikachu is powerful pokemon!");
                System.out.println("Pikachu HP: " + pikachu.getHp() + "\nPikachu Damage" + pikachu.getDamage() + "\nPikachu ability: Multicast electric charge that hits from 10 to 40 damage");
                pic.drawPikachu();
                System.out.println("Let's get started");
                System.out.println("Choose command for Pikachu: \n1-Attack\n2-Defense\n3-UseSkill");
                break;
            case 2:
                System.out.println("Great Choice! SlowPoke is powerful pokemon!");
                System.out.println("SlowPoke HP: " + slowPoke.getHp() + "\nSlowPoke Damage " + slowPoke.getDamage() + "\nSlowPoke ability: The enemy attacks himself with one of the attacking abilities (the ability is chosen randomly)");
                pic.drawSlowPoke();
                System.out.println("Let's get started");
                System.out.println("Choose command for SlowPoke: \n1-Attack\n2-Defense\n3-UseSkill");
                break;
        }

        boolean pikachuSkillIsUsed = false;
        boolean slowpokeSkillIsUsed = false;
        boolean pokemonIsAlive = true;
        while (pokemonIsAlive) {
            System.out.println("Your turn!");
            commandForAbility = in.nextInt();
            while (commandForAbility <= 0 || commandForAbility > 3) {
                System.out.println("Unknow command! Enter correct command for ability!");
                commandForAbility = in.nextInt();
            }
            if (commandForChoosePokemon == 1) {
                switch (commandForAbility) {
                    case 1:
                        System.out.println("Pikachu attack!!!");
                        pikachu.attack(slowPoke, pikachu);
                        System.out.println("Good punch! SlowPoke HP: " + slowPoke.getHp());
                        break;
                    case 2:
                        System.out.println("Good defense! Enemy miss! Your HP: " + pikachu.getHp());
                        continue;
                    case 3:
                        while(pikachuSkillIsUsed) {
                            System.out.println("Pikachu already used the ability, enter other action!");
                            commandForAbility = in.nextInt();
                            if(commandForAbility != 3) {
                                break;
                            }
                        }
                        System.out.println("Pikachu use electric charge");
                        pikachu.ElectricСharge(slowPoke, pikachu);
                        pikachuSkillIsUsed = true;
                        break;
                }
                if (slowPoke.getHp() <= 0) {
                    pic.drawPicachuWin();
                    pokemonIsAlive = false;
                    break;
                } else {
                    System.out.println("SlowPoke turn!");
                    slowPoke.attack(pikachu, slowPoke);
                    System.out.println("Pikachu HP: " + pikachu.getHp());
                    if(pikachu.getHp() <= 0) {
                        pic.drawSlowPokeWin();
                        pokemonIsAlive = false;
                        break;
                    }
                }
            } else {
                switch (commandForAbility) {
                    case 1:
                        System.out.println("SlowPoke attack!!!");
                        slowPoke.attack(pikachu, slowPoke);
                        System.out.println("Good punch! Pikachu HP: " + pikachu.getHp());
                        break;
                    case 2:
                        System.out.println("Good defense! Enemy miss! Your HP: " + slowPoke.getHp());
                        continue;
                    case 3:
                        System.out.println("Slowpoke use Psycho Slam");
                        slowPoke.psychoSlam(pikachu);
                        slowpokeSkillIsUsed = true;
                        break;
                }
                if (pikachu.getHp() <= 0) {
                    pic.drawSlowPokeWin();
                    pokemonIsAlive = false;
                    break;
                } else {
                    System.out.println("Pikachu turn!");
                    pikachu.attack(slowPoke, pikachu);
                    System.out.println("SlowPoke HP: " + slowPoke.getHp());
                    if(slowPoke.getHp() <= 0) {
                        pic.drawPicachuWin();
                        pokemonIsAlive = false;
                        break;
                    }
                }
            }

        }
    }
}
